﻿namespace Project
{
    partial class VPersona
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.BtnNuevo = new System.Windows.Forms.Button();
            this.BtnModificar = new System.Windows.Forms.Button();
            this.BtnEliminar = new System.Windows.Forms.Button();
            this.TxbSApellido = new System.Windows.Forms.TextBox();
            this.TxbSNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpFechadeNacimientoA = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.TxbPApellido = new System.Windows.Forms.TextBox();
            this.TxbHijos = new System.Windows.Forms.TextBox();
            this.TxbPNombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LblHijos = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CmbBuscar = new System.Windows.Forms.ComboBox();
            this.TxbBuscar = new System.Windows.Forms.TextBox();
            this.DataPersona = new System.Windows.Forms.DataGridView();
            this.BtnDireccion = new System.Windows.Forms.Button();
            this.BtnTelefono = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbEstadoCivil = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbMunicipio = new System.Windows.Forms.ComboBox();
            this.CmbDepartamento = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.CmbDomicilio = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.MtbCedula = new System.Windows.Forms.MaskedTextBox();
            this.LblId = new System.Windows.Forms.Label();
            this.BtnMunicipio = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataPersona)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnGuardar);
            this.panel1.Controls.Add(this.BtnNuevo);
            this.panel1.Controls.Add(this.BtnModificar);
            this.panel1.Controls.Add(this.BtnEliminar);
            this.panel1.Location = new System.Drawing.Point(32, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(112, 302);
            this.panel1.TabIndex = 5;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardar.Image = global::Project.Properties.Resources.cancelar;
            this.BtnGuardar.Location = new System.Drawing.Point(14, 224);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(86, 60);
            this.BtnGuardar.TabIndex = 3;
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnNuevo
            // 
            this.BtnNuevo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnNuevo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnNuevo.Image = global::Project.Properties.Resources.usuario__1_;
            this.BtnNuevo.Location = new System.Drawing.Point(14, 14);
            this.BtnNuevo.Name = "BtnNuevo";
            this.BtnNuevo.Size = new System.Drawing.Size(86, 66);
            this.BtnNuevo.TabIndex = 0;
            this.BtnNuevo.UseVisualStyleBackColor = true;
            this.BtnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // BtnModificar
            // 
            this.BtnModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnModificar.Image = global::Project.Properties.Resources.usuario__2_;
            this.BtnModificar.Location = new System.Drawing.Point(14, 83);
            this.BtnModificar.Name = "BtnModificar";
            this.BtnModificar.Size = new System.Drawing.Size(86, 63);
            this.BtnModificar.TabIndex = 1;
            this.BtnModificar.UseVisualStyleBackColor = true;
            this.BtnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEliminar.Image = global::Project.Properties.Resources.usuario;
            this.BtnEliminar.Location = new System.Drawing.Point(14, 154);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.Size = new System.Drawing.Size(86, 64);
            this.BtnEliminar.TabIndex = 2;
            this.BtnEliminar.UseVisualStyleBackColor = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // TxbSApellido
            // 
            this.TxbSApellido.Location = new System.Drawing.Point(339, 162);
            this.TxbSApellido.Name = "TxbSApellido";
            this.TxbSApellido.Size = new System.Drawing.Size(228, 20);
            this.TxbSApellido.TabIndex = 83;
            // 
            // TxbSNombre
            // 
            this.TxbSNombre.Location = new System.Drawing.Point(339, 84);
            this.TxbSNombre.Name = "TxbSNombre";
            this.TxbSNombre.Size = new System.Drawing.Size(228, 20);
            this.TxbSNombre.TabIndex = 81;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(153, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 20);
            this.label2.TabIndex = 93;
            this.label2.Text = "Segundo Apellido:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(165, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 20);
            this.label3.TabIndex = 92;
            this.label3.Text = "Segundo Nombre:";
            // 
            // DtpFechadeNacimientoA
            // 
            this.DtpFechadeNacimientoA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpFechadeNacimientoA.Location = new System.Drawing.Point(339, 290);
            this.DtpFechadeNacimientoA.Name = "DtpFechadeNacimientoA";
            this.DtpFechadeNacimientoA.Size = new System.Drawing.Size(228, 20);
            this.DtpFechadeNacimientoA.TabIndex = 87;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(150, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(183, 20);
            this.label4.TabIndex = 91;
            this.label4.Text = "Fecha de Nacimiento:";
            // 
            // TxbPApellido
            // 
            this.TxbPApellido.Location = new System.Drawing.Point(339, 125);
            this.TxbPApellido.Name = "TxbPApellido";
            this.TxbPApellido.Size = new System.Drawing.Size(228, 20);
            this.TxbPApellido.TabIndex = 82;
            // 
            // TxbHijos
            // 
            this.TxbHijos.Location = new System.Drawing.Point(339, 207);
            this.TxbHijos.Name = "TxbHijos";
            this.TxbHijos.Size = new System.Drawing.Size(228, 20);
            this.TxbHijos.TabIndex = 86;
            this.TxbHijos.TextChanged += new System.EventHandler(this.TxbDireccion_TextChanged);
            this.TxbHijos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbHijos_KeyPress);
            // 
            // TxbPNombre
            // 
            this.TxbPNombre.Location = new System.Drawing.Point(339, 50);
            this.TxbPNombre.Name = "TxbPNombre";
            this.TxbPNombre.Size = new System.Drawing.Size(228, 20);
            this.TxbPNombre.TabIndex = 80;
            this.TxbPNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbPNombre_KeyPress);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(177, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 20);
            this.label5.TabIndex = 90;
            this.label5.Text = "Primer Apellido:";
            // 
            // LblHijos
            // 
            this.LblHijos.AutoSize = true;
            this.LblHijos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblHijos.Location = new System.Drawing.Point(204, 207);
            this.LblHijos.Name = "LblHijos";
            this.LblHijos.Size = new System.Drawing.Size(103, 20);
            this.LblHijos.TabIndex = 89;
            this.LblHijos.Text = "Nº de Hijos:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(186, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 20);
            this.label7.TabIndex = 88;
            this.label7.Text = "Primer Nombre:";
            // 
            // CmbBuscar
            // 
            this.CmbBuscar.FormattingEnabled = true;
            this.CmbBuscar.Items.AddRange(new object[] {
            "Primer Nombre",
            "Primer Apellido",
            "Cédula",
            "Domicilio",
            "Estado Civil",
            "Municipio",
            "Departamento"});
            this.CmbBuscar.Location = new System.Drawing.Point(251, 360);
            this.CmbBuscar.Name = "CmbBuscar";
            this.CmbBuscar.Size = new System.Drawing.Size(162, 21);
            this.CmbBuscar.TabIndex = 106;
            this.CmbBuscar.SelectedIndexChanged += new System.EventHandler(this.CmbBuscar_SelectedIndexChanged);
            // 
            // TxbBuscar
            // 
            this.TxbBuscar.Location = new System.Drawing.Point(427, 361);
            this.TxbBuscar.Multiline = true;
            this.TxbBuscar.Name = "TxbBuscar";
            this.TxbBuscar.Size = new System.Drawing.Size(279, 20);
            this.TxbBuscar.TabIndex = 105;
            this.TxbBuscar.TextChanged += new System.EventHandler(this.TxbBuscar_TextChanged);
            this.TxbBuscar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxbBuscar_KeyUp);
            // 
            // DataPersona
            // 
            this.DataPersona.AllowUserToAddRows = false;
            this.DataPersona.AllowUserToDeleteRows = false;
            this.DataPersona.AllowUserToOrderColumns = true;
            this.DataPersona.AllowUserToResizeColumns = false;
            this.DataPersona.AllowUserToResizeRows = false;
            this.DataPersona.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataPersona.BackgroundColor = System.Drawing.Color.Silver;
            this.DataPersona.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataPersona.Location = new System.Drawing.Point(32, 396);
            this.DataPersona.Name = "DataPersona";
            this.DataPersona.Size = new System.Drawing.Size(902, 170);
            this.DataPersona.TabIndex = 108;
            this.DataPersona.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataPersona_CellClick);
            this.DataPersona.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataPersona_CellContentClick);
            // 
            // BtnDireccion
            // 
            this.BtnDireccion.BackColor = System.Drawing.SystemColors.MenuBar;
            this.BtnDireccion.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnDireccion.Location = new System.Drawing.Point(691, 276);
            this.BtnDireccion.Name = "BtnDireccion";
            this.BtnDireccion.Size = new System.Drawing.Size(243, 30);
            this.BtnDireccion.TabIndex = 109;
            this.BtnDireccion.Text = "Dirección";
            this.BtnDireccion.UseVisualStyleBackColor = false;
            this.BtnDireccion.Click += new System.EventHandler(this.BtnDireccion_Click);
            // 
            // BtnTelefono
            // 
            this.BtnTelefono.BackColor = System.Drawing.SystemColors.MenuBar;
            this.BtnTelefono.Location = new System.Drawing.Point(691, 240);
            this.BtnTelefono.Name = "BtnTelefono";
            this.BtnTelefono.Size = new System.Drawing.Size(243, 30);
            this.BtnTelefono.TabIndex = 110;
            this.BtnTelefono.Text = "Teléfono";
            this.BtnTelefono.UseVisualStyleBackColor = false;
            this.BtnTelefono.Click += new System.EventHandler(this.BtnTelefono_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(199, 251);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 20);
            this.label1.TabIndex = 111;
            this.label1.Text = "Estado Civil:";
            // 
            // CmbEstadoCivil
            // 
            this.CmbEstadoCivil.FormattingEnabled = true;
            this.CmbEstadoCivil.Items.AddRange(new object[] {
            "Soltero",
            "Casado"});
            this.CmbEstadoCivil.Location = new System.Drawing.Point(339, 249);
            this.CmbEstadoCivil.Name = "CmbEstadoCivil";
            this.CmbEstadoCivil.Size = new System.Drawing.Size(228, 21);
            this.CmbEstadoCivil.TabIndex = 112;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(617, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 114;
            this.label6.Text = "Municipio:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // CmbMunicipio
            // 
            this.CmbMunicipio.FormattingEnabled = true;
            this.CmbMunicipio.Location = new System.Drawing.Point(721, 186);
            this.CmbMunicipio.Name = "CmbMunicipio";
            this.CmbMunicipio.Size = new System.Drawing.Size(161, 21);
            this.CmbMunicipio.TabIndex = 115;
            this.CmbMunicipio.SelectedIndexChanged += new System.EventHandler(this.CmbMunicipio_SelectedIndexChanged);
            // 
            // CmbDepartamento
            // 
            this.CmbDepartamento.FormattingEnabled = true;
            this.CmbDepartamento.Location = new System.Drawing.Point(721, 140);
            this.CmbDepartamento.Name = "CmbDepartamento";
            this.CmbDepartamento.Size = new System.Drawing.Size(213, 21);
            this.CmbDepartamento.TabIndex = 117;
            this.CmbDepartamento.SelectedIndexChanged += new System.EventHandler(this.CmbDepartamento_SelectedIndexChanged);
            this.CmbDepartamento.RightToLeftChanged += new System.EventHandler(this.CmbDepartamento_RightToLeftChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(577, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 20);
            this.label8.TabIndex = 116;
            this.label8.Text = "Departamento:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(636, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 20);
            this.label9.TabIndex = 121;
            this.label9.Text = "Cédula:";
            // 
            // CmbDomicilio
            // 
            this.CmbDomicilio.FormattingEnabled = true;
            this.CmbDomicilio.Items.AddRange(new object[] {
            "Casa Propia",
            "Alquilada",
            "Familiar"});
            this.CmbDomicilio.Location = new System.Drawing.Point(721, 47);
            this.CmbDomicilio.Name = "CmbDomicilio";
            this.CmbDomicilio.Size = new System.Drawing.Size(213, 21);
            this.CmbDomicilio.TabIndex = 123;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(617, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 20);
            this.label10.TabIndex = 122;
            this.label10.Text = "Domicilio:";
            // 
            // MtbCedula
            // 
            this.MtbCedula.Location = new System.Drawing.Point(721, 98);
            this.MtbCedula.Mask = "000-000000-0000L";
            this.MtbCedula.Name = "MtbCedula";
            this.MtbCedula.Size = new System.Drawing.Size(213, 20);
            this.MtbCedula.TabIndex = 124;
            // 
            // LblId
            // 
            this.LblId.AutoSize = true;
            this.LblId.Location = new System.Drawing.Point(919, 360);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(15, 13);
            this.LblId.TabIndex = 125;
            this.LblId.Text = "id";
            this.LblId.Visible = false;
            this.LblId.Click += new System.EventHandler(this.LblId_Click);
            // 
            // BtnMunicipio
            // 
            this.BtnMunicipio.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BtnMunicipio.Image = global::Project.Properties.Resources.nuevo;
            this.BtnMunicipio.Location = new System.Drawing.Point(888, 178);
            this.BtnMunicipio.Name = "BtnMunicipio";
            this.BtnMunicipio.Size = new System.Drawing.Size(46, 34);
            this.BtnMunicipio.TabIndex = 118;
            this.BtnMunicipio.UseVisualStyleBackColor = false;
            this.BtnMunicipio.Click += new System.EventHandler(this.BtnMunicipio_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Project.Properties.Resources.lupa3;
            this.pictureBox1.Location = new System.Drawing.Point(181, 337);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(55, 53);
            this.pictureBox1.TabIndex = 107;
            this.pictureBox1.TabStop = false;
            // 
            // VPersona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(980, 585);
            this.Controls.Add(this.LblId);
            this.Controls.Add(this.MtbCedula);
            this.Controls.Add(this.CmbDomicilio);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.BtnMunicipio);
            this.Controls.Add(this.CmbDepartamento);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CmbMunicipio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CmbEstadoCivil);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnTelefono);
            this.Controls.Add(this.BtnDireccion);
            this.Controls.Add(this.DataPersona);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.CmbBuscar);
            this.Controls.Add(this.TxbBuscar);
            this.Controls.Add(this.TxbSApellido);
            this.Controls.Add(this.TxbSNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DtpFechadeNacimientoA);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxbPApellido);
            this.Controls.Add(this.TxbHijos);
            this.Controls.Add(this.TxbPNombre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LblHijos);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Name = "VPersona";
            this.Text = "Persona";
            this.Load += new System.EventHandler(this.VPersona_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataPersona)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.Button BtnNuevo;
        private System.Windows.Forms.Button BtnModificar;
        private System.Windows.Forms.Button BtnEliminar;
        private System.Windows.Forms.TextBox TxbSApellido;
        private System.Windows.Forms.TextBox TxbSNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpFechadeNacimientoA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxbPApellido;
        private System.Windows.Forms.TextBox TxbHijos;
        private System.Windows.Forms.TextBox TxbPNombre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblHijos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox CmbBuscar;
        private System.Windows.Forms.TextBox TxbBuscar;
        private System.Windows.Forms.DataGridView DataPersona;
        private System.Windows.Forms.Button BtnDireccion;
        private System.Windows.Forms.Button BtnTelefono;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbEstadoCivil;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CmbMunicipio;
        private System.Windows.Forms.ComboBox CmbDepartamento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnMunicipio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CmbDomicilio;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox MtbCedula;
        private System.Windows.Forms.Label LblId;
    }
}

