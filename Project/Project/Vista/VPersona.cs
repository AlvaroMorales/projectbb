﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Project.Modelo;
using Project.Vista;
using System.Collections;

namespace Project
{
    public partial class VPersona : Form
    {
        Validaciones val = new Validaciones();

        public delegate void Pasar(string dato);
        public delegate void Pasar2(string dato);
        public event Pasar pasado;
        public event Pasar2 pasado2;

        public ArrayList ar = new ArrayList();
        public ArrayList tel = new ArrayList();

        public VPersona()
        {
            InitializeComponent();
            BtnGuardar.Enabled = false;
            BtnEliminar.Enabled = false;
            CmbBuscar.SelectedIndex = 0;
            CmbEstadoCivil.SelectedIndex = 0;
            CmbDomicilio.SelectedIndex = 0;
            HabilitarDatos(false);
            Mostrar();
            LlenarDepart();
            LlenarMunicipio();
            
        }
        private void LlenarDepart()
        {
            MPersona.LlenarDepartamento(CmbDepartamento);
        }
        private void LlenarMunicipio()
        {
            MPersona.LlenarMunicipio(CmbMunicipio,CmbDepartamento.Text);
        }


        void OcultarColumnas()
        {
            DataPersona.Columns[0].Visible = false;
        }

        public bool nuevo = false, act = false, guardar=false;
        
        private void Mostrar()
        {
            this.DataPersona.DataSource = MPersona.Mostrar();
            this.OcultarColumnas();
        }

        void HabilitarDatos(bool var)
        {
            TxbPNombre.Enabled = var;
            TxbPNombre.Text = "";
            TxbSNombre.Enabled = var;
            TxbSNombre.Text = "";
            TxbPApellido.Enabled = var;
            TxbPApellido.Text = "";
            TxbSApellido.Enabled = var;
            TxbSApellido.Text = "";
            TxbHijos.Text = "";
            MtbCedula.Text = "";
            MtbCedula.Enabled = var;
            CmbDomicilio.Enabled = var;
            TxbHijos.Enabled = var;
            CmbEstadoCivil.Enabled = var;
            DtpFechadeNacimientoA.Enabled = var;
            CmbMunicipio.Enabled = var;
            CmbDepartamento.Enabled = var;
            BtnTelefono.Enabled = var;
            BtnDireccion.Enabled = var;
            BtnMunicipio.Enabled = var;
          
        }
        void Cancelar()
        {
            nuevo = false;
            act = false;
            guardar = false;
            BtnNuevo.Enabled = true;
            BtnModificar.Enabled = true;
            BtnEliminar.Enabled = false;
            BtnGuardar.Enabled = false;
            HabilitarDatos(false);
            ar.Clear();
            tel.Clear();
        }
        void Nuevo()
        {
            nuevo = true;
            act = false;
            guardar = false;
            BtnNuevo.Enabled = false;
            BtnModificar.Enabled = false;
            BtnEliminar.Enabled = true;
            BtnGuardar.Enabled = true;
            HabilitarDatos(true);
        }
        void Act()
        {
            nuevo = false;
            act = true;
            guardar = false;
            BtnNuevo.Enabled = false;
            BtnModificar.Enabled = false;
            BtnEliminar.Enabled = true;
            BtnGuardar.Enabled = true;
            HabilitarDatos(true);
            GridDireccion();
           
        }


        private void VPersona_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Nuevo();

        }

        private void TxbDireccion_TextChanged(object sender, EventArgs e)
        {

        }
      
        private void BtnDireccion_Click(object sender, EventArgs e)
        {

            VDireccion vd = new VDireccion();
            if (act)
            {
                vd.LblID.Text = LblId.Text;
            }
           
            vd.pasado += new VDireccion.Pasar(Ejecutar);
            vd.LblPersona.Text = TxbPNombre.Text + TxbPApellido.Text;
            vd.ShowDialog();
                 

        }
        public void Ejecutar(string valor)
        {
            ar.Add(valor);
        }
        public void Ejecutar2(string valor)
        {
            tel.Add(valor);
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            Act();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (TxbPNombre.Text == "" || TxbPApellido.Text == "" || CmbMunicipio.Text=="")
            {
                MensajeError("Verifique los datos por favor");
            }
            else
            {
                try
                {
                    string rpta = "";
                    int x = 0;

                    if (nuevo)
                    {
                        rpta = MPersona.Insertar(TxbPNombre.Text, TxbSNombre.Text, TxbPApellido.Text, TxbSApellido.Text, MtbCedula.Text, DtpFechadeNacimientoA.Value, CmbEstadoCivil.SelectedItem.ToString(), x, CmbDomicilio.SelectedItem.ToString(), CmbMunicipio.Text);
                        if (ar.Count > 0)
                        {
                            for (int i = 0; i < ar.Count; i++)
                            {
                                rpta = MDireccion.Insertar(ar[i].ToString());
                                rpta = MDireccion.InsertarRelacion();
                            }
                        }
                        if (tel.Count > 0)
                        {
                            for (int i = 0; i < tel.Count; i++)
                            {
                                rpta = MTelefono.Insertar(Convert.ToInt32(tel[i].ToString()));
                                rpta = MTelefono.InsertarRelacion();
                            }
                        }

                        Mostrar();
                    }
                    else
                    {
                        if (act)
                        {
                            rpta = MPersona.Editar(Convert.ToInt32(LblId.Text), TxbPNombre.Text, TxbSNombre.Text, TxbPApellido.Text, TxbSApellido.Text, MtbCedula.Text, DtpFechadeNacimientoA.Value, CmbEstadoCivil.SelectedItem.ToString(), Convert.ToInt32(TxbHijos.Text.ToString()), CmbDomicilio.SelectedItem.ToString(), CmbMunicipio.Text);

                            if (ar.Count > 0)
                            {
                                for (int i = 0; i < ar.Count; i++)
                                {
                                    rpta = MDireccion.Insertar(ar[i].ToString());

                                    rpta = MDireccion.InsertarExistente(Convert.ToInt32(LblId.Text));

                                }
                            }
                            if (tel.Count > 0)
                            {
                                for (int i = 0; i < tel.Count; i++)
                                {
                                    rpta = MTelefono.Insertar(Convert.ToInt32(tel[i].ToString()));

                                    rpta = MTelefono.InsertarExistente(Convert.ToInt32(LblId.Text));

                                }
                            }


                            Mostrar();
                        }

                    }


                    if (rpta.Equals("OK"))
                    {
                        if (this.nuevo)
                        {
                            this.MensajeOk("Se Insertó de forma correcta el registro");
                        }

                    }
                    else
                    {
                        this.MensajeError(rpta);
                    }

                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace);
                }
            }
            Cancelar();    
        } 
        private void MensajeOk(string mensaje)
        {
            MessageBox.Show(mensaje, "Project Persona", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        private void MensajeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Project Persona", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void CmbMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbPNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void TxbHijos_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.SoloNúmeros(e);
        }

        private void DataPersona_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (act)
            {
                GridDireccion();
            }

        }

        private void BtnTelefono_Click(object sender, EventArgs e)
        {
            VTelefono vt = new VTelefono();
            if (act)
            {
                vt.LblID.Text = LblId.Text;
            }

            vt.pasado += new VTelefono.Pasar(Ejecutar2);
            vt.LblPersona.Text = TxbPNombre.Text + TxbPApellido.Text;
            vt.ShowDialog();
        }

        private void DataPersona_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnMunicipio_Click(object sender, EventArgs e)
        {
            VMunicipio mm = new VMunicipio();
            mm.LblDepartamento.Text = CmbDepartamento.Text;
            mm.ShowDialog();
            LlenarMunicipio();
        }

        private void LblId_Click(object sender, EventArgs e)
        {

        }

        private void CmbDepartamento_RightToLeftChanged(object sender, EventArgs e)
        {

        }

        private void CmbDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            LlenarMunicipio();
        }

        private void CmbBuscar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TxbBuscar.Text == "")
            {
                Mostrar();
            }
            else
            {
                Buscar(TxbBuscar.Text);
            }
        }

        private void TxbBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            if (TxbBuscar.Text == "")
            {
                Mostrar();
            }
            else
            {
                Buscar(TxbBuscar.Text);
            }

        }
        void Buscar(string dato)
        {
            if (CmbBuscar.Text=="Primer Nombre")
            {
                DataPersona.DataSource = MPersona.BuscarPorPrimerNombre(dato);
            }
            if (CmbBuscar.Text == "Primer Apellido")
            {
                DataPersona.DataSource = MPersona.BuscarPorPApellido(dato);
            }
            if (CmbBuscar.Text == "Domicilio")
            {
                DataPersona.DataSource = MPersona.BuscarPorDomicilio(dato);
            }
            if (CmbBuscar.Text == "Cédula")
            {
                DataPersona.DataSource = MPersona.BuscarPorCedula(dato);
            }
            if (CmbBuscar.Text == "Estado Civil")
            {
                DataPersona.DataSource = MPersona.BuscarPorECivil(dato);
            }
            if (CmbBuscar.Text == "Municipio")
            {
                DataPersona.DataSource = MPersona.BuscarPorMunicipio(dato);
            }
            if (CmbBuscar.Text == "Departamento")
            {
                DataPersona.DataSource = MPersona.BuscarPorDepartamento(dato);
            }
        }

        private void TxbBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Cancelar();
        }
        public void GridDireccion()
        {
            
                LblId.Text = DataPersona.CurrentRow.Cells[0].Value.ToString();
                TxbPNombre.Text = DataPersona.CurrentRow.Cells[1].Value.ToString();
                TxbSNombre.Text = DataPersona.CurrentRow.Cells[2].Value.ToString();
                TxbPApellido.Text = DataPersona.CurrentRow.Cells[3].Value.ToString();
                TxbSApellido.Text = DataPersona.CurrentRow.Cells[4].Value.ToString();
                MtbCedula.Text = DataPersona.CurrentRow.Cells[5].Value.ToString();
                CmbMunicipio.Text = DataPersona.CurrentRow.Cells[6].Value.ToString();
                CmbDepartamento.Text = DataPersona.CurrentRow.Cells[7].Value.ToString();
                DtpFechadeNacimientoA.Value = Convert.ToDateTime(DataPersona.CurrentRow.Cells[8].Value.ToString());
                CmbEstadoCivil.Text = DataPersona.CurrentRow.Cells[9].Value.ToString();
                TxbHijos.Text = DataPersona.CurrentRow.Cells[10].Value.ToString();
                CmbDomicilio.Text = DataPersona.CurrentRow.Cells[11].Value.ToString();
            
        }
    }
}
