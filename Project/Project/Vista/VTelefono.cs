﻿using Project.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.Vista
{
    public partial class VTelefono : Form
    {
        public VTelefono()
        {
            InitializeComponent();
        }
        public delegate void Pasar(string dato);
        public event Pasar pasado;


        private void Mostrar(int Idpersona)
        {

            this.DataTelefono.DataSource = MTelefono.Mostrar(Idpersona);
            this.OcultarColumnas();
        }

        void OcultarColumnas()
        {
            DataTelefono.Columns[0].Visible = false;
        }


        private void VTelefono_Load(object sender, EventArgs e)
        {
            if (LblID.Text != "")
            {
                Mostrar(Convert.ToInt32(LblID.Text));
            }
            else
            {
                tabPage1.Parent = null;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (TxbDireccion.Text=="")
            {
                MessageBox.Show("Debe ingresar un teléfono");
            }
            else
            {
                VPersona vp = new VPersona();
                pasado(TxbDireccion.Text);
                DataTelefonoNueva.Rows.Add(LblPersona.Text, TxbDireccion.Text);

                TxbDireccion.Text = "";
            }
           
        }

        private void TxbDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validaciones val = new Validaciones();
            val.SoloNúmeros(e);
        }
    }
}
