﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Project.Vista;
using Project.Modelo;


namespace Project.Vista
{
    public partial class VDireccion : Form
    {
        public VDireccion()
        {
            InitializeComponent();
           
        }
        public delegate void Pasar(string dato);
        public event Pasar pasado;

        private void Mostrar(int Idpersona)
        {
           
            this.DataDireccion.DataSource = MDireccion.Mostrar(Idpersona);
            this.OcultarColumnas();
        }

        void OcultarColumnas()
        {
            DataDireccion.Columns[0].Visible = false;
        }


        void AgregarDireccion(string nombre, string direc)
        {
            DataDireccionNueva.Rows.Add(nombre, direc);
        }
        

        void HabilitarDatos(bool cond)
        {
            TxbDireccion.Enabled = cond;
            TxbDireccion.Text = "";

        }
        void limpiar()
        {
            TxbDireccion.Text = "";
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
        
        }

        private void VDireccion_Load(object sender, EventArgs e)
        {
            if (LblID.Text != "")
            {
                Mostrar(Convert.ToInt32(LblID.Text));
            }
            else
            {
                tabPage1.Parent = null;
            }

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
           
        }


        private void button1_Click(object sender, EventArgs e)
        {
         
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
           
        }

        private void DataDireccion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAgregar_Click_1(object sender, EventArgs e)
        {
            if (TxbDireccion.Text=="")
            {
                MessageBox.Show("Debe ingresar una dirección");
            }
            else
            {
                VPersona vp = new VPersona();
                pasado(TxbDireccion.Text);
                DataDireccionNueva.Rows.Add(LblPersona.Text, TxbDireccion.Text);

                TxbDireccion.Text = "";
            }
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
