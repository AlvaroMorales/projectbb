﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Project.Modelo;

namespace Project.Vista
{
    public partial class VMunicipio : Form
    {
        public VMunicipio()
        {
            InitializeComponent();
        }

        private void DataDepartamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void Mostrar(string Departamento)
        {
            this.DataMunicipio.DataSource = MMunicipio.Mostrar(Departamento);
        }

        private void VMunicipio_Load(object sender, EventArgs e)
        {
            Mostrar(LblDepartamento.Text);
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            if (TxbDireccion.Text=="")
            {
                MessageBox.Show("Debe ingresar un municipio");
            }
            else
            {
                string rpta = MMunicipio.Insertar(TxbDireccion.Text, LblDepartamento.Text);
                Mostrar(LblDepartamento.Text);
                TxbDireccion.Text = "";
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
