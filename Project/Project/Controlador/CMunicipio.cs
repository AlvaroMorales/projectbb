﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.Controlador
{
    class CMunicipio
    {
        private string Municipio;
        private string Departamento;

        public string Municipio1
        {
            get
            {
                return Municipio;
            }

            set
            {
                Municipio = value;
            }
        }

        public string Departamento1
        {
            get
            {
                return Departamento;
            }

            set
            {
                Departamento = value;
            }
        }

        public CMunicipio(string municipio, string departamento)
        {
            Municipio1 = municipio;
            Departamento1 = departamento;
        }

        public CMunicipio()
        {
        }

        public string Insertar(CMunicipio Municipio)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_InsertarMunicipio";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado


                SqlParameter ParMunicipio = new SqlParameter();
                ParMunicipio.ParameterName = "@Municipio";
                ParMunicipio.SqlDbType = SqlDbType.VarChar;
                ParMunicipio.Size = 40;
                ParMunicipio.Value = Municipio.Municipio1;
                SqlCmd.Parameters.Add(ParMunicipio);

                SqlParameter ParDepartamento = new SqlParameter();
                ParDepartamento.ParameterName = "@Departamento";
                ParDepartamento.SqlDbType = SqlDbType.VarChar;
                ParDepartamento.Size = 40;
                ParDepartamento.Value = Municipio.Departamento1;
                SqlCmd.Parameters.Add(ParDepartamento);


                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }

        public DataTable Mostrar(CMunicipio Municipio)
        {
            DataTable DtResultado = new DataTable("Municipio");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_MostrarMunicipios";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParDepartamento = new SqlParameter();
                ParDepartamento.ParameterName = "@Departamento";
                ParDepartamento.SqlDbType = SqlDbType.VarChar;
                ParDepartamento.Size = 40;
                ParDepartamento.Value = Municipio.Departamento1;
                SqlCmd.Parameters.Add(ParDepartamento);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }

        





    }
}
