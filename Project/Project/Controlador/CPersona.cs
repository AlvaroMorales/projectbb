﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.Controlador
{
    class CPersona
    {
        private int IdPersona;
        private string PrimerNombre;
        private string SegundoNombre;
        private string PApellido;
        private string SApellido;
        private int Hijos;
        private string domicilio;
        private string estadocivil;
        private DateTime FechaNac;
        private string IdMunicipio;
        private String direccion;
        private string Cedula;
        private PictureBox Foto;

        public CPersona()
        { }

        public CPersona(int idPersona, string primerNombre, string segundoNombre, string pApellido, string sApellido, int hijos, string domicilio, string estadocivil, DateTime fechaNac, string idMunicipio, string direccion,string cedula, PictureBox foto)
        {
            IdPersona = idPersona;
            PrimerNombre = primerNombre;
            SegundoNombre = segundoNombre;
            PApellido = pApellido;
            SApellido = sApellido;
            Hijos = hijos;
            this.domicilio = domicilio;
            this.estadocivil = estadocivil;
            FechaNac = fechaNac;
            IdMunicipio = idMunicipio;
            this.direccion = direccion;
            this.Cedula1 = cedula;
            this.Foto = foto;
        }

        public int IdPersona1
        {
            get
            {
                return IdPersona;
            }

            set
            {
                IdPersona = value;
            }
        }

        public string PrimerNombre1
        {
            get
            {
                return PrimerNombre;
            }

            set
            {
                PrimerNombre = value;
            }
        }

        public string SegundoNombre1
        {
            get
            {
                return SegundoNombre;
            }

            set
            {
                SegundoNombre = value;
            }
        }

        public string PApellido1
        {
            get
            {
                return PApellido;
            }

            set
            {
                PApellido = value;
            }
        }

        public string SApellido1
        {
            get
            {
                return SApellido;
            }

            set
            {
                SApellido = value;
            }
        }

        public int Hijos1
        {
            get
            {
                return Hijos;
            }

            set
            {
                Hijos = value;
            }
        }

        public string Domicilio
        {
            get
            {
                return domicilio;
            }

            set
            {
                domicilio = value;
            }
        }

        public string Estadocivil
        {
            get
            {
                return estadocivil;
            }

            set
            {
                estadocivil = value;
            }
        }

        public DateTime FechaNac1
        {
            get
            {
                return FechaNac;
            }

            set
            {
                FechaNac = value;
            }
        }

        public string IdMunicipio1
        {
            get
            {
                return IdMunicipio;
            }

            set
            {
                IdMunicipio = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Cedula1
        {
            get
            {
                return Cedula;
            }

            set
            {
                Cedula = value;
            }
        }

        public PictureBox Foto1
        {
            get
            {
                return Foto;
            }

            set
            {
                Foto = value;
            }
        }

        public string Insertar(CPersona Persona)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_InsertarPersona";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado


                SqlParameter ParpNombre = new SqlParameter();
                ParpNombre.ParameterName = "@PrimerNombre";
                ParpNombre.SqlDbType = SqlDbType.VarChar;
                ParpNombre.Size = 20;
                ParpNombre.Value = Persona.PrimerNombre1;
                SqlCmd.Parameters.Add(ParpNombre);

                SqlParameter ParsNombre = new SqlParameter();
                ParsNombre.ParameterName = "@SegundoNombre";
                ParsNombre.SqlDbType = SqlDbType.VarChar;
                ParsNombre.Size = 20;
                ParsNombre.Value = Persona.SegundoNombre1;
                SqlCmd.Parameters.Add(ParsNombre);

                SqlParameter ParPApellido = new SqlParameter();
                ParPApellido.ParameterName = "@PrimerApellido";
                ParPApellido.SqlDbType = SqlDbType.VarChar;
                ParPApellido.Size = 20;
                ParPApellido.Value = Persona.PApellido1;
                SqlCmd.Parameters.Add(ParPApellido);

                SqlParameter ParsApellido = new SqlParameter();
                ParsApellido.ParameterName = "@SegundoApellido";
                ParsApellido.SqlDbType = SqlDbType.VarChar;
                ParsApellido.Size = 20;
                ParsApellido.Value = Persona.PApellido1;
                SqlCmd.Parameters.Add(ParsApellido);

                SqlParameter ParnContacto = new SqlParameter();
                ParnContacto.ParameterName = "@Cedula";
                ParnContacto.SqlDbType = SqlDbType.VarChar;
                ParnContacto.Size = 20;
                ParnContacto.Value = Persona.Cedula1;
                SqlCmd.Parameters.Add(ParnContacto);

                SqlParameter Parfecha = new SqlParameter();
                Parfecha.ParameterName = "@FechaNacimiento";
                Parfecha.SqlDbType = SqlDbType.Date;
                Parfecha.Size = 60;
                Parfecha.Value = Persona.FechaNac1;
                SqlCmd.Parameters.Add(Parfecha);

                SqlParameter ParEstadoCivil = new SqlParameter();
                ParEstadoCivil.ParameterName = "@EstadoCivil";
                ParEstadoCivil.SqlDbType = SqlDbType.VarChar;
                ParEstadoCivil.Size = 60;
                ParEstadoCivil.Value = Persona.Estadocivil;
                SqlCmd.Parameters.Add(ParEstadoCivil);

                SqlParameter ParHijos = new SqlParameter();
                ParHijos.ParameterName = "@Hijos";
                ParHijos.SqlDbType = SqlDbType.Int;
                ParHijos.Size = 60;
                ParHijos.Value = Persona.Hijos1;
                SqlCmd.Parameters.Add(ParHijos);

                SqlParameter ParDomicilio = new SqlParameter();
                ParDomicilio.ParameterName = "@Domicilio";
                ParDomicilio.SqlDbType = SqlDbType.VarChar;
                ParDomicilio.Size = 60;
                ParDomicilio.Value = Persona.Domicilio;
                SqlCmd.Parameters.Add(ParDomicilio);

                SqlParameter ParMunicipio = new SqlParameter();
                ParMunicipio.ParameterName = "@Municipio";
                ParMunicipio.SqlDbType = SqlDbType.VarChar;
                ParMunicipio.Size = 30;
                ParMunicipio.Value = Persona.IdMunicipio1;
                SqlCmd.Parameters.Add(ParMunicipio);

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }
        public DataTable Mostrar()
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_MostrarPersona";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }

        public string Editar(CPersona Persona)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_EditarPersona";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado

                SqlParameter ParIdPersona = new SqlParameter();
                ParIdPersona.ParameterName = "@IdPersona";
                ParIdPersona.SqlDbType = SqlDbType.Int;
                ParIdPersona.Size = 20;
                ParIdPersona.Value = Persona.IdPersona1;
                SqlCmd.Parameters.Add(ParIdPersona);

                SqlParameter ParpNombre = new SqlParameter();
                ParpNombre.ParameterName = "@PrimerNombre";
                ParpNombre.SqlDbType = SqlDbType.VarChar;
                ParpNombre.Size = 20;
                ParpNombre.Value = Persona.PrimerNombre1;
                SqlCmd.Parameters.Add(ParpNombre);

                SqlParameter ParsNombre = new SqlParameter();
                ParsNombre.ParameterName = "@SegundoNombre";
                ParsNombre.SqlDbType = SqlDbType.VarChar;
                ParsNombre.Size = 20;
                ParsNombre.Value = Persona.SegundoNombre1;
                SqlCmd.Parameters.Add(ParsNombre);

                SqlParameter ParPApellido = new SqlParameter();
                ParPApellido.ParameterName = "@PrimerApellido";
                ParPApellido.SqlDbType = SqlDbType.VarChar;
                ParPApellido.Size = 20;
                ParPApellido.Value = Persona.PApellido1;
                SqlCmd.Parameters.Add(ParPApellido);

                SqlParameter ParsApellido = new SqlParameter();
                ParsApellido.ParameterName = "@SegundoApellido";
                ParsApellido.SqlDbType = SqlDbType.VarChar;
                ParsApellido.Size = 20;
                ParsApellido.Value = Persona.SApellido1;
                SqlCmd.Parameters.Add(ParsApellido);

                SqlParameter ParnContacto = new SqlParameter();
                ParnContacto.ParameterName = "@Cedula";
                ParnContacto.SqlDbType = SqlDbType.VarChar;
                ParnContacto.Size = 20;
                ParnContacto.Value = Persona.Cedula1;
                SqlCmd.Parameters.Add(ParnContacto);

                SqlParameter Parfecha = new SqlParameter();
                Parfecha.ParameterName = "@FechaNacimiento";
                Parfecha.SqlDbType = SqlDbType.Date;
                Parfecha.Size = 60;
                Parfecha.Value = Persona.FechaNac1;
                SqlCmd.Parameters.Add(Parfecha);

                SqlParameter ParEstadoCivil = new SqlParameter();
                ParEstadoCivil.ParameterName = "@EstadoCivil";
                ParEstadoCivil.SqlDbType = SqlDbType.VarChar;
                ParEstadoCivil.Size = 60;
                ParEstadoCivil.Value = Persona.Estadocivil;
                SqlCmd.Parameters.Add(ParEstadoCivil);

                SqlParameter ParHijos = new SqlParameter();
                ParHijos.ParameterName = "@Hijos";
                ParHijos.SqlDbType = SqlDbType.Int;
                ParHijos.Size = 60;
                ParHijos.Value = Persona.Hijos1;
                SqlCmd.Parameters.Add(ParHijos);

                SqlParameter ParDomicilio = new SqlParameter();
                ParDomicilio.ParameterName = "@Domicilio";
                ParDomicilio.SqlDbType = SqlDbType.VarChar;
                ParDomicilio.Size = 60;
                ParDomicilio.Value = Persona.Domicilio;
                SqlCmd.Parameters.Add(ParDomicilio);

                SqlParameter ParMunicipio = new SqlParameter();
                ParMunicipio.ParameterName = "@Municipio";
                ParMunicipio.SqlDbType = SqlDbType.VarChar;
                ParMunicipio.Size = 30;
                ParMunicipio.Value = Persona.IdMunicipio1;
                SqlCmd.Parameters.Add(ParMunicipio);

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }

        public void LlenarDepartamento(ComboBox cm)
        {
            SqlConnection SqlCon;
            SqlCommand cmd;
            SqlDataReader dr;
            try
            {
                cm.Items.Clear();
                SqlCon = new SqlConnection(Conexion.Cn);
                SqlCon.Open();
                cmd = new SqlCommand("Select Distinct Departamento from Departamento",SqlCon);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cm.Items.Add(dr["Departamento"].ToString());
                }
                SqlCon.Close();
                dr.Close();
                cm.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Llenar Departamento: " + ex.ToString());
            }
        }
        public void LlenarMunicipio(ComboBox cm,string departamento)
        {
            SqlConnection SqlCon;
            SqlCommand cmd;
            SqlDataReader dr;
            try
            {
                cm.Items.Clear();
                SqlCon = new SqlConnection(Conexion.Cn);
                SqlCon.Open();
                cmd = new SqlCommand("Select Distinct Municipio as 'Municipio' from Municipio m join Departamento d on m.IdDepartamento = d.IdDepartamento where d.Departamento = '" + departamento+"'", SqlCon);
                int cont = 0;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cont++;
                        cm.Items.Add(dr["Municipio"].ToString());
                    
                }
                SqlCon.Close();
                dr.Close();
                if (cont>0)
                {
                    cm.SelectedIndex = 0;
                }
                else
                {
                    cm.ResetText();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Llenar Municipio: " + ex.ToString());
            }
        }

        public DataTable BuscarPorPrimerNombre(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaPNombre";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@PNombre";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size =30;
                ParBusqueda.Value = persona.PrimerNombre1;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }

        public DataTable BuscarPorPrimerApellido(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaPApellido";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = persona.PApellido1;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }
        public DataTable BuscarPorCedula(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaCedula";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = persona.Cedula1;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }
        public DataTable BuscarPorEstadoCivil(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaECivil";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = persona.Estadocivil;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }
        public DataTable BuscarPorDomicilio(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaDomicilio";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = persona.Domicilio;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }
        public DataTable BuscarPorMunicipio(CPersona persona)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaMunicipio";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = persona.IdMunicipio1;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }
        public DataTable BuscarPorDepartamento(string  dep)
        {
            DataTable DtResultado = new DataTable("Persona");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_BuscarPersonaDepartamento";
                SqlCmd.CommandType = CommandType.StoredProcedure;
                SqlParameter ParBusqueda = new SqlParameter();
                ParBusqueda.ParameterName = "@Dato";
                ParBusqueda.SqlDbType = SqlDbType.VarChar;
                ParBusqueda.Size = 30;
                ParBusqueda.Value = dep;
                SqlCmd.Parameters.Add(ParBusqueda);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }


    }
}
