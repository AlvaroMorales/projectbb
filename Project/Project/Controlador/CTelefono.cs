﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Controlador
{
    class CTelefono
    {

        private int IdTelefono;
        private int Telefono;
        private int IdPersona;

        public int IdTelefono1
        {
            get
            {
                return IdTelefono;
            }

            set
            {
                IdTelefono = value;
            }
        }

        public int Telefono1
        {
            get
            {
                return Telefono;
            }

            set
            {
                Telefono = value;
            }
        }

        public int IdPersona1
        {
            get
            {
                return IdPersona;
            }

            set
            {
                IdPersona = value;
            }
        }

        public CTelefono()
        {
        }

        public CTelefono(int idTelefono, int telefono, int idPersona)
        {
            IdTelefono1 = idTelefono;
            Telefono1 = telefono;
            IdPersona1 = idPersona;
        }

        public string Insertar(CTelefono Telefono)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_InsertarTelefono";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                // Parámetros del Procedimiento Almacenado


                SqlParameter ParTelefono = new SqlParameter();
                ParTelefono.ParameterName = "@Telefono";
                ParTelefono.SqlDbType = SqlDbType.VarChar;
                ParTelefono.Size = 100;
                ParTelefono.Value = Telefono.Telefono1;
                SqlCmd.Parameters.Add(ParTelefono);


                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }

        public string InsertarRelacion()
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_RelacionTelefono";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }

        public string InsertarRelacionExistente(CTelefono Telefono)
        {
            string rpta = "";
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                //Código
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCon.Open();
                //Establecer el Comando
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_RelacionTelExis";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //Parámetros
                SqlParameter ParIdPersona = new SqlParameter();
                ParIdPersona.ParameterName = "@IdPersona";
                ParIdPersona.SqlDbType = SqlDbType.Int;
                ParIdPersona.Value = Telefono.IdPersona1;
                SqlCmd.Parameters.Add(ParIdPersona);

                //Ejecutamos nuestro comando

                rpta = SqlCmd.ExecuteNonQuery() == 1 ? "OK" : "NO se Ingreso el Registro";
            }
            catch (Exception ex)
            {
                rpta = ex.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return rpta;

        }
        public DataTable Mostrar(CTelefono Telefono)
        {
            DataTable DtResultado = new DataTable("Direccion");
            SqlConnection SqlCon = new SqlConnection();
            try
            {
                SqlCon.ConnectionString = Conexion.Cn;
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "sp_MostrarTelefono";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ParIdPersona = new SqlParameter();
                ParIdPersona.ParameterName = "@IdPersona";
                ParIdPersona.SqlDbType = SqlDbType.Int;
                ParIdPersona.Value = Telefono.IdPersona1;
                SqlCmd.Parameters.Add(ParIdPersona);


                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;
            }
            return DtResultado;

        }

    }
}
