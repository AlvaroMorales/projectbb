﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Controlador;
using System.Windows.Forms;
using System.Data;

namespace Project.Modelo
{
    class MPersona
    {
        public static string Insertar(string PNombre, string SNombre, string PApellido, string SApellido, string Cedula, DateTime FechaNac, String estadocivil, int Hijos,string Domicilio,string municipio)
        {
            CPersona Obj = new CPersona();
            Obj.PrimerNombre1 = PNombre;
            Obj.SegundoNombre1 = SNombre;
            Obj.PApellido1 = PApellido;
            Obj.SApellido1 = SApellido;
            Obj.Cedula1 = Cedula;
            Obj.FechaNac1 = FechaNac;
            Obj.Estadocivil = estadocivil;
            Obj.Hijos1 = Hijos;
            Obj.Domicilio = Domicilio;
            Obj.IdMunicipio1 = municipio;
            return Obj.Insertar(Obj);
        }
        public static DataTable Mostrar()
        {
            return new CPersona().Mostrar();
        }
        public static string Editar(int IdPersona, string PNombre, string SNombre, string PApellido, string SApellido, string Cedula, DateTime FechaNac, string estadocivil, int Hijos, string Domicilio, string municipio)
        { 
            
            CPersona Obj = new CPersona();
            Obj.IdPersona1 = IdPersona;
            Obj.PrimerNombre1 = PNombre;
            Obj.SegundoNombre1 = SNombre;
            Obj.PApellido1 = PApellido;
            Obj.SApellido1 = SApellido;
            Obj.Cedula1 = Cedula;
            Obj.FechaNac1 = FechaNac;
            Obj.Estadocivil = estadocivil;
            Obj.Hijos1 = Hijos;
            Obj.Domicilio = Domicilio;
            Obj.IdMunicipio1 = municipio;

            return Obj.Editar(Obj);
        }
        public static void LlenarDepartamento(ComboBox cm)
        {
            CPersona Cm = new CPersona();
            Cm.LlenarDepartamento(cm);
        }

        public static void LlenarMunicipio(ComboBox cm,string d)
        {
            CPersona Cm = new CPersona();
            Cm.LlenarMunicipio(cm,d);
        }

        public static DataTable BuscarPorPrimerNombre(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.PrimerNombre1 = dato;
            return new CPersona().BuscarPorPrimerNombre(Obj);
          
        }
        public static DataTable BuscarPorPApellido(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.PApellido1 = dato;
            return new CPersona().BuscarPorPrimerApellido(Obj);
        }
        public static DataTable BuscarPorCedula(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.Cedula1 = dato;
            return new CPersona().BuscarPorCedula(Obj);
        }
        public static DataTable BuscarPorDomicilio(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.Domicilio = dato;
            return new CPersona().BuscarPorDomicilio(Obj);
        }
        public static DataTable BuscarPorECivil(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.Estadocivil = dato;
            return new CPersona().BuscarPorEstadoCivil(Obj);
        }
        public static DataTable BuscarPorMunicipio(string dato)
        {
            CPersona Obj = new CPersona();
            Obj.IdMunicipio1 = dato;
            return new CPersona().BuscarPorMunicipio(Obj);
        }
        public static DataTable BuscarPorDepartamento(string dato)
        {
            return new CPersona().BuscarPorDepartamento(dato);
        }

    }
}
