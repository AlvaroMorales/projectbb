﻿using Project.Controlador;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Modelo
{
    class MTelefono
    {
        public static string Insertar(int Telefono)
        {
            CTelefono Obj = new CTelefono();
            Obj.Telefono1 = Telefono;

            return Obj.Insertar(Obj);
        }
        public static string InsertarExistente( int idpersona)
        {
            CTelefono Obj = new CTelefono();
            Obj.IdPersona1 = idpersona;

            return Obj.InsertarRelacionExistente(Obj);
        }

        public static string InsertarRelacion()
        {
            CTelefono Obj = new CTelefono();
            return Obj.InsertarRelacion();
        }
        public static DataTable Mostrar(int IdPersona)
        {
            CTelefono Obj = new CTelefono();
            Obj.IdPersona1 = IdPersona;
            return new CTelefono().Mostrar(Obj);
        }
    }
}
