﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Controlador;
using System.Data;

namespace Project.Modelo
{
    class MDireccion
    {
        public static string Insertar(string Direccion)
        {
            CDireccion Obj = new CDireccion();
            Obj.Direccion1 = Direccion;

            return Obj.Insertar(Obj);
        }
        public static string InsertarExistente(int idpersona)
        {
            CDireccion Obj = new CDireccion();
            Obj.IdPersona1 = idpersona;

            return Obj.InsertarRelacionExistente(Obj);
        }

        public static string InsertarRelacion()
        {
            CDireccion Obj = new CDireccion();
            return Obj.InsertarRelacion();
        }
        public static DataTable Mostrar(int IdPersona)
        {
            CDireccion Obj = new CDireccion();
            Obj.IdPersona1 = IdPersona;
            return new CDireccion().Mostrar(Obj);
        }
    }
   
}
