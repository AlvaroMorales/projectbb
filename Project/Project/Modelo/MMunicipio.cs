﻿using Project.Controlador;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project.Modelo
{
    class MMunicipio
    {
        public static DataTable Mostrar(string Departamento)
        {
            CMunicipio Obj = new CMunicipio();
            Obj.Departamento1 = Departamento;
            return new CMunicipio().Mostrar(Obj);
        }
        public static string Insertar(string Municipio, string Departamento)
        {
            CMunicipio Obj = new CMunicipio();
            Obj.Municipio1 = Municipio;
            Obj.Departamento1 = Departamento;

            return Obj.Insertar(Obj);
        }

    }
}
